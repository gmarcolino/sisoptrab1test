package main_project;

import java.util.*;

public class servidor extends Thread {

	Random numero_do_arquivo = new Random();
	Random tamanho_do_arquivo = new Random();

	// ids dos servers
	public static int idH = 0;
	public static int idJ = 1;

	// ids das threads

	public String nome;
	public long tempo_de_espera;
	public int id_de_dir;

	// lista guardada pelo servidor
	public List<Integer> lista_de_arquivos_servidor = new ArrayList();

	// Permissoes de leitura e escrita

	public Leitura_E_Escrita[] permissoes_de_LeE;

	public servidor(String name, int id) {
		this.nome = name;
		this.id_de_dir = id;
		this.tempo_de_espera = 0;
		this.permissoes_de_LeE = new Leitura_E_Escrita[Main.numero_de_arquivos];

		for (int x = 0; x < Main.numero_de_arquivos; x++) {
			this.permissoes_de_LeE[x] = new Leitura_E_Escrita();
		}
	}

	public void run() {

		int arquivo_num = this.numero_do_arquivo.nextInt(Main.numero_de_arquivos);

		System.out.println("Numero do arquivo" + arquivo_num);

		for (int x = 0; x < Main.alteracoes; x++) {
			try {
				Main.semaforo_de_sincronizacao[arquivo_num].acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				this.modifica(this.id_de_dir, arquivo_num);
				this.sincroniza(this.id_de_dir, arquivo_num);
				Main.semaforo_de_sincronizacao[arquivo_num].release();
			}
		}

	}

	public void modifica(int id_server, int id_arq) {

		int novo_tamanho = this.tamanho_do_arquivo.nextInt(500);
		System.out.println("Modificando serv " + id_server + " no arq " + id_arq);

		try {
			Main.servidores.get((id_server + 1) % 2).permissoes_de_LeE[id_arq].trancar_escrita();
			Main.servidores.get(id_server).permissoes_de_LeE[id_arq].trancar_escrita();
			Main.servidores.get(id_server).lista_de_arquivos_servidor.set(id_arq, novo_tamanho);

		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			Main.servidores.get(id_server).permissoes_de_LeE[id_arq].destrancar_escrita();
		}

	}

	public void sincroniza(int id_server, int id_arq) {

		System.out.println("Sincronizando serv " + id_server + " no arq " + id_arq);
		int novo_tamanho = Main.servidores.get(id_server).lista_de_arquivos_servidor.get(id_arq);

		try {
			Main.servidores.get(id_server).permissoes_de_LeE[id_arq].trancar_leitura();
			Main.servidores.get((id_server + 1) % 2).lista_de_arquivos_servidor.set(id_arq, novo_tamanho);
			this.tempo_de_espera += novo_tamanho;
			Thread.sleep((long) novo_tamanho);

		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			Main.servidores.get(id_server).permissoes_de_LeE[id_arq].destrancar_leitura();
			Main.servidores.get((id_server + 1) % 2).permissoes_de_LeE[id_arq].destrancar_escrita();
		}
	}
}
