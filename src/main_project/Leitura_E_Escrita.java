package main_project;

public class Leitura_E_Escrita {

	public int leitores = 0;
	public int escritores = 0;
	public int pedidos_de_escrita = 0;

	public synchronized void trancar_leitura() throws InterruptedException {
		while (this.escritores > 0 || this.pedidos_de_escrita > 0) {
			wait();
		}
		this.leitores++;
	}

	public synchronized void trancar_escrita() throws InterruptedException {
		this.pedidos_de_escrita++;
		while (this.escritores > 0 || this.leitores > 0) {

			wait();
		}
		this.escritores++;
		this.pedidos_de_escrita--;
	}

	public synchronized void destrancar_leitura() {
		this.leitores--;
		notifyAll();
	}

	public synchronized void destrancar_escrita() {
		this.escritores--;
		notifyAll();
	}

}
