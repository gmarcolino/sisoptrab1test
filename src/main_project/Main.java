package main_project;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Semaphore;

public class Main {

	public static int numero_de_arquivos;
	public static int threads_num;
	public static int thread_req_num;
	public static int pastas = 2;
	public static int alteracoes;
	public static List<Integer> lista_de_Arquivos_servidorH = new ArrayList();
	public static List<Integer> lista_de_Arquivos_servidorJ = new ArrayList();
	public static List<trabalhadores> threads_trabalhadores = new ArrayList();
	public static List<servidor> servidores = new ArrayList();
	public static Semaphore[] semaforo_de_sincronizacao;

	public static void main(String[] args) throws IOException {

		// Inicializar as variasveis
		// ********************************************************************************

		Main.numero_de_arquivos = 10;
		Main.threads_num = 5;
		Main.thread_req_num = 2;
		Main.alteracoes = 10;
		Main.semaforo_de_sincronizacao = new Semaphore[Main.numero_de_arquivos];
		Random tamanho_do_arquivo = new Random();
		servidores.add(new servidor("HYDE", 0));
		servidores.add(new servidor("JEKYLL", 1));
		// Inicializar os arquivos

		System.out.println("Arquivos sendo carregados");
		for (int x = 0; x < Main.numero_de_arquivos; x++) {

			int tamanho = tamanho_do_arquivo.nextInt(100);
			Main.lista_de_Arquivos_servidorH.add(x, tamanho);
			Main.lista_de_Arquivos_servidorJ.add(x, tamanho);
			Main.semaforo_de_sincronizacao[x] = new Semaphore(1, true);

		}
		System.out.println("Arquivos carregados");
		// Adcionar arquivos no servidor

		System.out.println("Arquivos sendo carregados no servidor");
		servidores.get(0).lista_de_arquivos_servidor.addAll(lista_de_Arquivos_servidorH);
		servidores.get(1).lista_de_arquivos_servidor.addAll(lista_de_Arquivos_servidorJ);
		System.out.println("Arquivos carregados no servidor");

		// Finalizado inicializacao de variaveis
		// ******************************************************************

		// Inicializar threads servidor e trabalhador

		System.out.println("Ligando servidor Hyde");
		servidores.get(0).start();
		System.out.println("Ligando servidor Jekyll");
		servidores.get(1).start();

		System.out.println("Inicializando threads trabalhadoras");
		for (int x = 0; x < Main.threads_num; x++) {
			Main.threads_trabalhadores.add(x, new trabalhadores(x));
			Main.threads_trabalhadores.get(x).start();
		}

		// Processa trabalhadores
		System.out.println("Processando threads trabalhadoras");

		for (int x = 0; x < Main.threads_num; x++) {
			try {
				Main.threads_trabalhadores.get(x).join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Processo: " + x + " finalizado.");
		}

		// Processa Servers

		System.out.println("Processando servidores");
		try {
			System.out.println("Servidor Hyde finalizando.");
			servidores.get(0).join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Servidor Hyde finalizado.");

		try {
			System.out.println("Servidor Jekyll finalizando.");
			servidores.get(1).join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("Servidor Jekyll finalizado.");
		
		System.out.println("FIM");
	}
}