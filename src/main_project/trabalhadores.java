package main_project;

import java.util.Random;

public class trabalhadores extends Thread {

	Random servidor_random = new Random();
	Random arquivo_random = new Random();

	// Identificacao da thread
	public int id;
	public long tempo_de_bloqueio;

	public trabalhadores(int numero) {
		this.id = numero;
		this.tempo_de_bloqueio = 0;
	}

	public void run() {

		for (int x = 0; x < Main.thread_req_num; x++) {
			int servidor_escolhido = servidor_random.nextInt(2);
			int arquivo_escolhido = arquivo_random.nextInt(Main.numero_de_arquivos);

			this.fazerLeitura(servidor_escolhido, arquivo_escolhido);
		}
	}

	public void fazerLeitura(int id_server, int id_arquivo) {

		long tempoDeEspera = Main.servidores.get(id_server).lista_de_arquivos_servidor.get(id_arquivo);
		System.out.println("Thread " + this.id + ": lendo arquivo " + id_arquivo + " do servidor: " + id_server);
		
		try {
			Main.servidores.get(id_server).permissoes_de_LeE[id_arquivo].trancar_leitura();
			System.out.println("Numero de leitores: " + Main.servidores.get(id_server).permissoes_de_LeE[id_arquivo].leitores);
			this.tempo_de_bloqueio += tempoDeEspera;
			try {
				Thread.sleep(tempoDeEspera);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			Main.servidores.get(id_server).permissoes_de_LeE[id_arquivo].destrancar_leitura();
		}
	}
}
